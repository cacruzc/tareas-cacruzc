#include <iostream>
#include <stdio.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <chrono>
#include <ctime>
#include <sys/time.h>

int main (void)
{
  struct timeval tv1,tv2,tv3,tv4;
  int n;
  double tLU,tQR;
  std::ofstream fs("gsl.dat");
 
for(int j=1;j<10;j++){
  n=2^j; 
  double A[n^n];
  double B[n]; 
  for(int i=0; i<n; i++) { 
  
    gsl_rng * r =gsl_rng_alloc(gsl_rng_rand48*r);     
    A[i]=r;
    gsl_rng_free(gsl_rng*r);
    
    gsl_rng * r =gsl_rng_alloc(gsl_rng_rand48*r);     
    B[i]=r;
    gsl_rng_free(gsl_rng*r);
  }
 A = gsl_matrix_alloc(n,n);
 gsl_matrix_set(A, n, n);
 gsl_matrix_view m=  gsl_matrix_view_array (A, n, n);
 gsl_vector_view b= gsl_vector_view_array (B, n);
 gsl_vector *x= gsl_vector_alloc (n);
 int s;

 //std::chrono::time_point<std::chrono::system_clock> start,end;
 // start = std::chrono::system_clock::now();
 
 gettimeofday(&tv1,NULL);
 gsl_linalg_LUdecomp (&m.matrix, p, &s);
 gsl_linalgLU_solve (&m.matrix, p, &b.vector, x);
 gettimeofday(&tv2,NULL);

// end=std::chrono::system_clock::now();
 

 //std::chrono::time_point<std::chrono::system_clock> start,end;
 //start = std::chrono::system_clock::now()
 
gettimeofday(&tv3,NULL);
gsl_linalg_QRdecomp (&m.matrix, p, &s);
gsl_linalgQR_solve (&m.matrix, p, &b.vector, x);
gettimeofday(&tv4,NULL); 

 //end=std::chrono::system_clock::now();
 
 // std::chrono::duration<double> elapsed_seconds = end-start; std::time_t end_time = std::chrono::system_clock::to_time_t(end);


 // std::chrono::duration<double> elapsed_seconds = end-start; std::time_t end_time = std::chrono::system_clock::to_time_t(end);

 // std::cout<<"finished computation at " <<std::ctime(&end_time)<<"elapsed time: "<<elapsed_second.count() <<"s\n";


fs << n << " "<< end_time << " "<< end_time <<endl;
 }
 std::fs.close();
 gsl_permutation_free (p);
 gsl_vector_free (x);
 printf(" Terminamos");
 return 0;
}
